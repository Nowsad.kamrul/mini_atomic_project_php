<?php
//asort function
$age=array("Peter"=>"35","Ben"=>"37","Joe"=>"43");
 asort($age);
 echo "<pre>";
 print_r($age);

 echo "<br>";

//compact function

 $firstname = "Nowsad";
$lastname = "Kamrul";
$age1 = "25";

$result = compact("firstname", "lastname", "age1");

print_r($result);

 echo "<br>";
// current function

 $people1 = array("Peter", "Joe", "Glenn", "Cleveland");

echo current($people1) . "<br>";

 echo "<br>";
//each function

 $people2 = array( "Joe", "Glenn", "Cleveland");
print_r (each($people2));
 echo "<br>"; 
//end function

$people3 = array("Peter", "Joe", "Glenn", "Cleveland");


echo end($people3); 
 echo "<br>";
 
//in_array() Function
 
 $people4 = array("Peter", "Joe", "Glenn", "Cleveland");

if (in_array("Nowsad", $people4))
  {
  echo "Match found";
  }
else
  {
  echo "Match not found";
  }

echo "<br>";
// key function

$people5=array("Peter","Joe","Glenn","Cleveland");
echo "The key from the current position is: " . key($people5);
 echo "<br>";
// list function
$my_array1= array("Abu","Nowsad","Kamrul");

list($a, $b, $c) = $my_array1;
echo "I have several names  $a,  $b and  $c.";

echo "<br>";
//next funnction
$people6 = array("Peter", "Joe", "Glenn", "Cleveland");

echo current($people6) . "<br>";
echo next($people6);

echo "<br>";
// pos function
$people7 = array("Peter", "Joe", "Glenn", "Cleveland");

echo pos($people7);
echo "<br>";
//prev() function

$people8 = array("Peter", "Joe", "Glenn", "Cleveland");

echo current($people8) . "<br>";
echo next($people8) . "<br>";
echo prev($people8);

echo "<br>";
//reset function

$people9 = array("Peter", "Joe", "Glenn", "Cleveland");

echo current($people9) . "<br>";
echo next($people9) . "<br>";

echo reset($people9);


echo "<br>";
//shuffle function
$my_array2 = array("red","green","blue","yellow","purple");

shuffle($my_array2);
print_r($my_array2);
echo "<br>";
//size of function

$cars1=array("Volvo","BMW","Toyota");
echo "<pre>";
echo sizeof($cars1);
echo "<br>";
//sort function
$cars2=array("Volvo","BMW","Toyota");
//sort($cars2);
echo sort($cars2);