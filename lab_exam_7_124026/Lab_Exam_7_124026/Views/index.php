
<?php
include_once ('../vendor/autoload.php');
$obj1 = new App\Terms();
$data = $obj1->index();
if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
    echo $_SESSION['msg'];
    unset($_SESSION['msg']);
}
?>
 <html>
    <head>        
        <title> Index | Data </title>
    </head>
    <body>
        <center>
 <h1> ____________________________Student Semester Registration Form___________________________</h1><br><br/>

    <style>
        body {
            background-color: white;
        }

        h1 {
            color: white;
            text-align: center;
        }



    </style>
    <div style="text-align:center"> 
   
    <br/>
    <a href="create.php">Add New Student Record</a><br/>
    
    </div>
   
<br/>
<table border="1" >
    <thead>
    <tr>
        
        <th>Id</th>
        <th>Name</th>
        <th>Semester</th>
        <th>Offer</th>
        <th>Cost</th>
        <th>Waiver</th>
        <th>Total Cost</th>
        
        <th colspan="3">Action</th>
    </tr>
    </thead>
    <tbody>
            <?php
            $serial = 1;
     if (isset($data) && !empty($data)){
        foreach ($data as $item) {
        ?>
        <tr>
             <td><?php echo $serial++ ?></td>
        <td><?php echo ucwords($item['name']); ?></td>
        
        <td><?php echo $item['semester']?></td>
          <td><?php echo $item['offer']?></td>
          <td><?php echo $item['cost']?></td>
          <td><?php echo $item['waiver']?></td>
          <td><?php echo $item['total']?></td> 
           

            

            <td><a href="show.php?id=<?php echo $item['unique_id']; ?>">View</a></td>
            <td><a href="edit.php?id=<?php echo $item['unique_id']; ?>" onclick="return confirm('Are you sure you want to Update student details?')">Update</a></td>
            <td><a href="delete.php?id=<?php echo $item['unique_id']; ?>" onclick="return confirm('Are you sure you want to delete student details?')">Delete</a></td>
        </tr>
         <?php }
    }else {
        ?>
        <tr>
            <td colspan="3">
                No available data
            </td>
        </tr>
    <?php } ?>
           </tbody>
          
</table>
        </center>
 </body>
    </html>


