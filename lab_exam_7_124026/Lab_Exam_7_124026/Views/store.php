<?php
include_once ('../vendor/autoload.php');
$obj = new App\Terms();



if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $obj->prepare($_POST);
    if (!empty($_POST["name"]) && !empty($_POST["semester"])) {
        
        $obj->store();
    } else {
        header('location:create.php');
    }

   
} else {
    $_SESSION['Message'] = "Opps Sorry you are not authorized for this page";
    header('location:error.php');
}
