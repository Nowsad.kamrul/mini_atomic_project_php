<?php

namespace App;

class Terms {

    public $id = "";
    public $name = "";
    public $offer = "";
    public $data = "";
    public $semester = "";
    public $cost = "";
    public $waiver = "";
    public $total = "";
    public $price = "";

    public function __construct() {
        session_start();
        $conn = mysql_connect('localhost', 'root', '') or die(mysql_error());
        $db = mysql_select_db('php124026') or die(mysql_error());
    }

    public function prepare($data = '') {
        if (array_key_exists('name', $data) && !empty($data['name'])) {
            $this->name = $data['name'];
        } else {

            $_SESSION['nameempty'] = "name is required";
        }

        if (array_key_exists('id', $data) && !empty($data['id'])) {
            $this->id = $data['id'];
        }

        if (array_key_exists('semester', $data) && !empty($data['semester'])) {
            $this->semester = $data['semester'];
        } else {
            $_SESSION['semesterempty'] = "Semester is required";
        }

        if (array_key_exists('offer', $data) && !empty($data['offer'])) {
            $this->offer = $data['offer'];
        }
        if ($this->semester == "1stSemester") {
            $this->price = 9000;
        }

        if ($this->semester == "2ndSemester") {
            $this->price = 11000;
        }

        if ($this->semester == "3rdSemester") {
            $this->price = 13000;
        }

        if ($this->offer == "Yes") {

            $this->waiver = ($this->price * 10) / 100;
        } else {
            $this->waiver = 0;
        }

        $this->total = $this->price - $this->waiver;
        return $this;
    }

    public function store() {

        $query = "INSERT INTO `php124026`.`student` (`id`, `name`, `semester`, `offer`, `cost`, `waiver`, `total`, `unique_id`) VALUES (NULL, '$this->name', '$this->semester', '$this->offer', '$this->price', '$this->waiver', '$this->total', '" . uniqid() . "')";

        if (mysql_query($query)) {
            $_SESSION['Message'] = "Data Successfully Submitted";
        }
        header('location:create.php');
    }

    public function index() {
        $qry = mysql_query("SELECT * FROM `student`");
        while ($row = mysql_fetch_assoc($qry)) {
            $this->data[] = $row;
        }return $this->data;
        return $this;
    }

    public function show() {
        $qry = "SELECT * FROM `student` WHERE unique_id=" . "'" . $this->id . "'";
        $result = mysql_query($qry);

        $row = mysql_fetch_assoc($result);

        return $row;
        return $this;
    }

    public function update() {

        $query = "UPDATE `student` SET `semester`='" . $this->semester . "', `offer` = '" . $this->offer . "', `cost` = $this->price, `waiver` = $this->waiver, `total` = $this->total WHERE `student`.`unique_id`=" . "'" . $this->id . "'";


        mysql_query($query);
        $_SESSION['msg'] = '<font color="green" size="5">Successfully Updated...  </font>';
        header("location: index.php");
        return $this;
    }

    public function delete() {
        $qry = "DELETE FROM `student` WHERE unique_id=" . "'" . $this->id . "'";
        mysql_query($qry);
        $_SESSION['msg'] = '<font color="green" size="5">Successfully Deleted </font>';
        header("location: index.php");
        return $this;
    }

}
