<?php

include ('../../../vendor/mpdf/mpdf/mpdf.php');
include_once ('../../../../vendor/autoload.php');

use App\Bitm\Seip124026\Mobile\Mobile;

$obj = new Mobile();
$alldata = $obj->index();
$trs = "";
$serial = 0;
foreach ($alldata as $data):

    $serial++;
    $trs.="<tr>";
    $trs.="<td>" .$serial. "</td>";
    $trs.="<td>" .$data['unique_id']. "</td>";
    $trs.="<td>" .$data['title']. "</td>";
    $trs.="<td>" .$data['laptop']. "</td>";
    $trs.="</tr>";

endforeach;
$html = <<<EOD
 
<!DOCTYPE html>
<html>
    <head>
        <title>List of Models</title>
        </head>
        <body>
        <h1>List Of Mobiles and Laptops</h1>
        <table border ="1">
        <thead>
        <tr>
        <th>Sl.</th>
           <th>Unique ID</th>
        <th>Mobile Model</th>
        <th>Laptop Model</th>
        </tr>
        </thead>
        <tbody>
         $trs;
        </tbody>
</table>        
EOD;
$mpdf= new mPDF();
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;
?>
