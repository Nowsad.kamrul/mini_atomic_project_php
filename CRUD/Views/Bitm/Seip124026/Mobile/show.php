<?php
//include_once '../../../../Src/Bitm/Seip124026/Mobile/Mobile.php';
include_once '../../../../vendor/autoload.php';
use App\Bitm\Seip124026\Mobile\Mobile;
$object = new Mobile();

$object->prepare($_GET);
$data= $object->show();
if (isset($data)&& !empty($data)){
?>
<head>
    <link rel="stylesheet" href="../../../../vendor/twitter/bootstrap/dist/css/bootstrap.min.css">
        <script src="jquery.min.js"></script>
        <script src = "../../../../vendor/twitter/bootstrap/dist/js/bootstrap.min.js"></script>
</head>
<body>
<center>
<a href="index.php">Back to list</a>
<table class="table table-hover">
     <thead>
    <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Unique Id</th>
         <th>Laptop Model</th>
    </tr>
    
    <tr>
        <td><?php echo $data['id']?></td>
        <td><?php echo $data['title']?></td>
          <td><?php echo $data['unique_id']?></td>
          <td><?php echo $data['laptop']?></td>
    </tr>
     </thead>
</table>
</center>    
</body>
<?php }else{
    $_SESSION['Message']="Not found,you are trying to show another page"."<a href= 'Create.php'>Create</a>";
    header('location:error.php');
}?>