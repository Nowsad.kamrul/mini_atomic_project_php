<?php
//
//error_reporting(E_ALL);
//error_reporting(E_ALL & ~E_DEPRECATED);
//ini_set('display_errors', true);
//ini_set('display_startup_errors', true);
//date_default_timezone_set('Asia/Dhaka');

if (PHP_SAPI == 'cli')
    die('This example should only be run from a web browser');

/* Include PHPExcel */
require_once '../../../../vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
require_once('../../../../vendor/autoload.php');

use App\Bitm\Seip124026\Mobile\Mobile;

$obj = new Mobile();

$alldata = $obj->index();

/* Create new PHPExcel object */
$objPHPExcel = new PHPExcel();

/* Set document properties */
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test Document for Office 2007 XLSX, generated using php Classes.")
    ->setKeywords(" Office 2007 openxml php")
    ->setCategory("Test result file");

/* add some data*/
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', 'SL')
    ->setCellValue('B1', 'Unique ID')
    ->setCellValue('C1', 'Mobile Model')
    ->setCellValue('D1', 'Laptop Model');
    

$counter = 2;
$serial = 0;

foreach ($alldata as $data) {
    $serial++;
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A' . $counter, $serial)
        ->setCellValue('B' . $counter, $data['unique_id'])
        ->setCellValue('C' . $counter, $data['title'])
        ->setCellValue('D' . $counter, $data['laptop']);
        
    $counter++;
}

/* rename worksheet */
$objPHPExcel->getActiveSheet()->setTitle("List of mobile entered data");

/* set active sheet index to the first sheet, so excel opens this as the first sheet*/
$objPHPExcel->setActiveSheetIndex(0);

/* redirect output to a client's web browser (Excel5)*/
header('Content-Type:application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="laptopmobiledata.xls"');
header('Content-Control: max-age=0');

/* If you're serving to IE over SSL, then the following msybe needed*/
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/ 1.1
header('Pragma: public'); // HTTP/ 1.1

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>



